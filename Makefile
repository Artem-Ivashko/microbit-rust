setup:
	sudo apt install gdb-multiarch \
		minicom \
		build-essential \
		libudev-dev # dependency for cargo embed, # build-essential is needed since Rust needs a C linker
	sudo usermod -a -G dialout ${USER} # Add the user to dialout group in order to use /dev/ttyACM0 without sudo. NOTE: change is active only after next log in or even after the PC reboot
	cp tool-files/.minirc.dfl ${HOME}/.minirc.dfl # copying minicom settings
	sudo cp tool-files/99-openocd.rules /etc/udev/rules.d/
	sudo udevadm control --reload-rules
	sudo usermod -a -G uucp ${USER}
# To check the installation, go to https://droogmic.github.io/microrust/setup/LINUX.html
	rustup target add thumbv7em-none-eabihf
	cargo install cargo-embed

clean:
	cargo clean -p test-microbit

format:
	rustfmt src/bin/*.rs

# cardo embed command should be run from a normal terminal (like GNOME terminal),
# otherwise it does not display the RTT output properly
hello:
	cargo embed --target thumbv7em-none-eabihf --bin hello

blink:
	cargo embed --target thumbv7em-none-eabihf --bin blink

# Should be open in a separate shell (not where cargo embed is running)
# Also make sure that halt_afterwards is set to true and rtt is disabled in Embed.toml
debug-blink:
	gdb-multiarch -x gdb-script target/thumbv7em-none-eabihf/debug/blink

serial:
	cargo embed --target thumbv7em-none-eabihf --bin serial

accelerometer:
	cargo embed --target thumbv7em-none-eabihf --bin accelerometer
