#![no_main]
#![no_std]

use cortex_m_rt::entry;
use panic_rtt_target as _;
use rtt_target::{rprintln, rtt_init_print};

use core::fmt::Write;

use microbit::{
    hal::prelude::*,
    hal::uarte,
};

fn display_message<T: Write>(serial: &mut T, string: &str)
{
    // We need to add \r here in order for the serial terminal to properly start new line
    write!(serial, "{}\r\n", string).unwrap();
    // Duplicating the UART output to RTT
    rprintln!("{}", string);
}

#[entry]
fn main() -> ! {
    rtt_init_print!();
    let board = microbit::Board::take().unwrap();

    let mut serial = uarte::Uarte::new(
        board.UARTE0,
        board.uart.into(),
        uarte::Parity::EXCLUDED,
        uarte::Baudrate::BAUD115200,
    );

    display_message(&mut serial, "Hello, do you see me?");

    let mut rx_buffer = [0u8];
    loop {
        serial.read(&mut rx_buffer).unwrap();
        display_message(&mut serial, "Echo:");
        display_message(&mut serial, core::str::from_utf8(&rx_buffer).unwrap());
    }
}
