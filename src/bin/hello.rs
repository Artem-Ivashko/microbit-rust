#![no_std]
#![no_main]

use panic_rtt_target as _;
use rtt_target::{rprintln, rtt_init_print};
// The import is unused, but otherwise the linker somehow cannot find the interrupt table
use microbit::hal::prelude::*;

use cortex_m_rt::entry;

#[entry]
fn main() -> ! {
    rtt_init_print!();
    rprintln!("Hello Embedded world, do you mind if I join?");
    loop {}
}
